using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using PolynomialObject.Exceptions;

namespace PolynomialObject
{

    public sealed class Polynomial : IEnumerable<PolynomialMember>, IEquatable<Polynomial>
    {

        public Polynomial() { }



        
        /// <summary>
        /// The number of members in polynomial
        /// </summary>
        public int Count { get; }


        /// <summary>
        /// Degree of polynomial
        /// </summary>
        public double Degree { get; }

        /// <summary>
        /// Adds new unique member to polynomial 
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException"></exception>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public void AddMember(PolynomialMember member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds new unique member to polynomial from tuple
        /// </summary>
        /// <param name="member">The member to be added</param>
        /// <exception cref="PolynomialArgumentException"></exception>
        public void AddMember((double degree, double coefficient) member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes member of specified degree
        /// </summary>
        /// <param name="degree">The degree of member to be deleted</param>
        /// <returns>True if member has been deleted</returns>
        public bool RemoveMember(double degree)
        {
            //todo
            throw  new NotImplementedException();
        }

        /// <summary>
        /// Searches the polynomial for a method of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>True if polynomial contains member</returns>
        public bool ContainsMember(double degree)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Finds member of specified degree
        /// </summary>
        /// <param name="degree">Degree of member</param>
        /// <returns>Returns the found member or null</returns>
        public PolynomialMember Find(double degree)
        {
            //todo
            throw new NotImplementedException();
        }

        public double this[double i]
        {
            get
            {
                //todo
                throw new NotImplementedException();
            }
            set 
            { 
                //todo
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Adds polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after adding</returns>
        public static Polynomial operator +(Polynomial a, (double degree, double coefficient) b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtract polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public static Polynomial operator -(Polynomial a, (double degree, double coefficient) b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies polynomial and tuple
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The tuple</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public static Polynomial operator *(Polynomial a, (double degree, double coefficient) b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds polynomial and polynomial member
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The polynomial member</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator +(Polynomial a, PolynomialMember b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtracts polynomial and polynomial member
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The polynomial member</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator -(Polynomial a, PolynomialMember b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies polynomial and polynomial member
        /// </summary>
        /// <param name="a">The polynomial</param>
        /// <param name="b">The polynomial member</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator *(Polynomial a, PolynomialMember b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>New polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator +(Polynomial a, Polynomial b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtracts two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator -(Polynomial a, Polynomial b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies two polynomials
        /// </summary>
        /// <param name="a">The first polynomial</param>
        /// <param name="b">The second polynomial</param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public static Polynomial operator *(Polynomial a, Polynomial b)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds tuple to polynomial
        /// </summary>
        /// <param name="member">The tuple to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        public Polynomial Add((double degree, double coefficient) member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtracts tuple from polynomial
        /// </summary>
        /// <param name="member">The tuple to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        public Polynomial Subtraction((double degree, double coefficient) member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies tuple with polynomial
        /// </summary>
        /// <param name="member">The tuple for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        public Polynomial Multiply((double degree, double coefficient) member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds member to polynomial
        /// </summary>
        /// <param name="member">The member to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Add(PolynomialMember member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtracts polynomial member from polynomial
        /// </summary>
        /// <param name="member">The polynomial member to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Subtraction(PolynomialMember member)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies polynomial member with polynomial
        /// </summary>
        /// <param name="member">The polynomial member for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Multiply(PolynomialMember member) => this * member;

        /// <summary>
        /// Adds polynomial to polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to add</param>
        /// <returns>Returns new polynomial after adding</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Add(Polynomial polynomial)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Subtracts polynomial from polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial to subtract</param>
        /// <returns>Returns new polynomial after subtraction</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Subtraction(Polynomial polynomial)
        {
            //todo
            throw new NotImplementedException();
        }

        /// <summary>
        /// Multiplies polynomial with polynomial
        /// </summary>
        /// <param name="polynomial">The polynomial for multiplication </param>
        /// <returns>Returns new polynomial after multiplication</returns>
        /// <exception cref="PolynomialArgumentNullException"></exception>
        public Polynomial Multiply(Polynomial polynomial)
        {
            //todo
            throw new NotImplementedException();
        }

        public IEnumerator<PolynomialMember> GetEnumerator()
        {
            //todo
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            //todo
            throw new NotImplementedException();
        }

        public bool Equals([AllowNull] Polynomial other)
        {
            //todo
            throw new NotImplementedException();
        }
    }
}
