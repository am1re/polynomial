using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PolynomialObject.Exceptions
{
    [Serializable]
    public class PolynomialIndexOutOfRangeException : Exception
    {
        public PolynomialIndexOutOfRangeException() { }

        public PolynomialIndexOutOfRangeException(string message) { }
    }
}
